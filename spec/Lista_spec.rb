require 'spec_helper'
require './lib/Lista/Bibliografia.rb'
require './lib/Lista/Lista.rb'

describe Lista do
  before :all do
    @l = Lista.new
    @a = Bibliografia.new
    @a.add_author("Carl Sagan")
    @b = Bibliografia.new
    @b.add_author("Neil De Grasse Tyson")
    @c = Bibliografia.new
    @c.add_author("Nikola Tesla")
  end
  
  describe "Inicialización de la lista" do
    it "Comprobar contenido" do
      expect(@l.First).to eq(nil)
    end
    
    it "Comprobar tamaño" do
      expect(@l.Sz).to eq(0)
    end
  end
  
  describe "Añadir contenido a la Lista" do
    it "Empujar contenido" do
      @l.push_back(@a)
      @l.push_back(@b)
      @l.push_back(@c)
    end
    
    it "Comprobar tamaño" do
      expect(@l.Sz).to eq(3)
    end
    
    it "Comprobar contenido" do
      expect(@l.at(0).Author[0]).to eq("Carl Sagan")
      expect(@l.at(1).Author[0]).to eq("Neil De Grasse Tyson")
      expect(@l.at(2).Author[0]).to eq("Nikola Tesla")
      expect(@l.at(3)).to eq(nil)
    end
  end
  
  describe "Extraer contenido" do
    it "Extraer en i" do
      expect(@l.takeAt(1).Author[0]).to eq("Neil De Grasse Tyson")
    end
    
    it "Extraer first" do
      expect(@l.takeFirst.Author[0]).to eq("Carl Sagan")
    end
    
    it "Comprobar tamaño" do
       expect(@l.Sz).to eq(1)
    end
  end
end
