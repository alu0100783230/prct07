Nodo = Struct.new(:value, :next)

class Lista 
    attr_reader :First, :Sz
    def initialize
        @First = nil
        @Sz = 0
    end
    
    def push_back(objeto)
        if(@Sz==0)
            @First = Nodo.new(objeto,nil)
            @Last = @First
        else
            @Last.next = Nodo.new(objeto,nil)
            @Last = @Last.next
        end
        @Sz = @Sz + 1
    end
    
    def at(i)
        @Aux = @First
        if(i<@Sz)
            for j in 1..i do
                @Aux = @Aux.next
            end
            @Aux.value
        else
            nil
        end
    end
    
    def takeAt(i)
        @Aux = @First
        if(i<@Sz)
            for j in 1..i-1 do
                @Aux = @Aux.next
            end
            @Aux2 = @Aux.next
            @Aux.next = @Aux2.next
            @Sz = @Sz -1
            @Aux2.value
        else
            nil
        end
    end
    
    def takeFirst
        if(@Sz>=1)
            @Aux = @First
            @First = @First.next
            @Sz = @Sz -1
            @Aux.value
        else
            nil
        end
    end
end
